## What is slink?

    Small Linker (SLINK) is a very small linker for linking slightly
    modified a.out object files into an executable.

## License

    All source code is Public Domain.

## Obtain the source code

    git clone https://git.candlhat.org/slink.git

## Building

    BSD:
    
        Make sure you have gcc and gmake installed then run gmake -f Makefile.unix.
    
    Linux:
    
        Make sure you have gcc and make installed then run make -f Makefile.unix.
    
    macOS:
    
        Make sure you have xcode command line tools installed then run
        make -f Makefile.unix.
    
    Windows:
    
        Make sure you have mingw installed and the location within your PATH variable
        then run mingw32-make.exe -f Makefile.w32.
